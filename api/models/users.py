from django.db import models


class User(models.Model):
    user_id = models.IntegerField(primary_key=True, null=True)
    creation_date = models.DateField(blank=True, null=True)
    update_date = models.DateField(blank=True, null=True)
    status = models.CharField(max_length=150, blank=True, null=True)
    firstname = models.CharField(max_length=150, blank=True, null=True)
    lastname = models.CharField(max_length=150, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    skills = models.CharField(max_length=1000, blank=True, null=True)
    cover_letter = models.CharField(max_length=150, blank=True, null=True)
    dismissed_jobs = models.CharField(max_length=50000, blank=True, null=True)

    def __str__(self):
        return self.email
