from django.db import models


class Skill(models.Model):
    skill_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True, null=True)
    name = models.CharField(max_length=150, blank=True, null=True)
    custom = models.NullBooleanField(default=True, null=True)

    def __str__(self):
        return self.name
