from django.db import models
from .users import User
from .jobs import Job


class Channel(models.Model):
    channel_id = models.IntegerField(primary_key=True, null=True)
    creation_date = models.DateField(blank=True, null=True)
    user_id = models.ForeignKey(User, null=True)
    job_id = models.IntegerField(Job, null=True)

    def __str__(self):
        return self.channel_id
