from django.db import models
from .channels import Channel


class Message(models.Model):
    message_id = models.IntegerField(primary_key=True, null=True)
    creation_date = models.DateField(blank=True, null=True)
    channel_id = models.ForeignKey(Channel, null=True)
    content = models.CharField(max_length=50000, blank=True, null=True)

    def __str__(self):
        return self.content
