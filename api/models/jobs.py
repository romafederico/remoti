from django.db import models


class Job(models.Model):
    job_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True, null=True)
    opening_date = models.CharField(max_length=150, blank=True, null=True)
    status = models.CharField(max_length=150, default="pending")
    source_id = models.CharField(max_length=150, blank=True, null=True)
    title = models.CharField(max_length=150, default="No title found")
    summary = models.TextField(max_length=10000, blank=True, null=True)
    description = models.TextField(max_length=50000, default="No description found")
    skills = models.CharField(max_length=1000, default="No skills found")
    employer_email = models.CharField(max_length=150, default="No email found", null=True)
    url = models.CharField(max_length=150, blank=True, null=True)
    job_type = models.CharField(max_length=150, blank=True, null=True)
    company = models.CharField(max_length=150, blank=True, null=True)
    company_logo = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        return self.title
