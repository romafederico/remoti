from rest_framework import serializers
from .models.skills import Skill
from .models.users import User
from .models.jobs import Job
from .models.channels import Channel
from .models.messages import Message


class SkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class JobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'


class ChannelsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = '__all__'


class MessagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
