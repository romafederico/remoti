from django.conf.urls import url
from .views.index import ApiIndex
from .views.skills import SkillsIndex
from .views.jobs import JobsIndex
from .views.github import GithubIndex

urlpatterns = (
    # /api/
    url(r'^$', ApiIndex.as_view(), name='index'),

    # api/skills
    url(r'^skills', SkillsIndex.as_view(), name='skills-index'),

    # api/jobs
    url(r'^jobs', JobsIndex.as_view(), name='jobs-index'),

    # api/import/github
    url(r'^import/github', GithubIndex.as_view(), name='github-index'),
)
