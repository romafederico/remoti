from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from ..serializers import JobsSerializer
from ..models.jobs import Job
import requests
import feedparser


class StackoverflowIndex(APIView):
    def get(self, request, format=None):
        url = 'https://stackoverflow.com/jobs/feed'

        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }

        jobs = requests.get(url, headers=headers).json()

        for job in jobs:

            saved_job = Job.objects.get(source_id=job['id']).source_id

            if saved_job != job['id']:

                j_email = 'need to parse from *how_to_apply*'
                j_summary = 'need to parse from *description*'
                j_skills = 'need to parse from skills table'
                j_status = 'need to parse from *email*'
                j_type = 'need to parse from *type*'

                j = {
                    "opening_date": job['created_at'],
                    "status": j_status,
                    "source_id": job['id'],
                    "title": job['title'],
                    "summary": j_summary,
                    "description": job['description'],
                    "skills": j_skills,
                    "employer_email": j_email,
                    "url": job['url'],
                    "job_type": j_type,
                    "company": job['company'],
                    "company_logo": job['company_logo']
                }

                serializer = JobsSerializer(data=j)

                if serializer.is_valid():
                    serializer.save()
                    print("Job imported")

                else:
                    print(serializer.errors)

            else:
                print('Existing Job')

        return Response({"message": "Github Jobs Imported"})
