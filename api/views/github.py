from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from ..serializers import JobsSerializer
from ..models.jobs import Job
from ..models.skills import Skill
import requests
import json
import re


class GithubIndex(APIView):
    def get(self, request):

        skills = Skill.objects.all()

        # HTTP Request creation
        url = 'https://jobs.github.com/positions.json'
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
        payload = {'page': 0}
        jobs = []
        enough_jobs = False

        # HTTP Request execution
        while enough_jobs is False:
            more_jobs = requests.get(url, headers=headers, params=payload).json()
            jobs.extend(more_jobs)

            if not more_jobs:
                enough_jobs = True
            else:
                payload['page'] = payload['page'] + 1

        # Loop through all jobs that came from the request
        count = 0
        for job in jobs:
            count += 1

            # Find job type
            remote_job = re.search(r'remote', str(job).lower())

            # Check job is remote and ID doesn't exist in database
            try:
                saved_job = Job.objects.get(source_id=job['id']).source_id
            except Job.DoesNotExist:
                saved_job = False

            if remote_job and not saved_job:

                # Extract email from job
                j_email = None
                email_match = re.search(r'[\w\.-]+@[\w\.-]+', job['how_to_apply'])
                if email_match:
                    j_email = email_match.group(0)
                    j_status = 'published'
                else:
                    j_status = 'pending'

                # Find job type
                j_type = None
                type_match = re.search(r'full', job['type'].lower())
                if type_match:
                    j_type = 'Full time'

                type_match = re.search(r'part', job['type'].lower())
                if type_match:
                    j_type = 'Part time'

                type_match = re.search(r'intern', job['type'].lower())
                if type_match:
                    j_type = 'Internship'

                # Assign skills to job
                j_skills = None

                # Extract paragraph summary
                j_summary = 'need to parse from *description*'

                # Create object to pass to serializer
                j = {
                    "opening_date": job['created_at'],
                    "status": j_status,
                    "source_id": job['id'],
                    "title": job['title'],
                    "summary": j_summary,
                    "description": job['description'],
                    "skills": j_skills,
                    "employer_email": j_email,
                    "url": job['url'],
                    "job_type": j_type,
                    "company": job['company'],
                    "company_logo": job['company_logo']
                }

                serializer = JobsSerializer(data=j)

                if serializer.is_valid():
                    # serializer.save()
                    print("-----Job imported")

                else:
                    print(serializer.errors)

            else:
                print(count, '-----Existing or Not Remote Job', job['id'])

        return Response({"message": "Github Jobs Imported"})
