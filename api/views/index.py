from rest_framework.views import APIView
from rest_framework.response import Response


class ApiIndex(APIView):
    def get(self, request):
        return Response({'message': 'Welcome to the Remoti API Index Endpoint'})
