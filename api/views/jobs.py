from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from ..models.jobs import Job
from ..serializers import *


class JobsIndex(APIView):
    def get(self, request, format=None):
        jobs = Job.objects.all()
        serializer = JobsSerializer(jobs, many=True)
        return Response(serializer.data)
